import AutoResizeAction from 'plugin/auto-resize-action';

describe("Auto resize action", function () {

  beforeEach(function(){
  });

  it("triggers the callack", function () {
    var spy = jasmine.createSpy('spy');
    var instance = new AutoResizeAction(spy);
    var dimensions = {w: '123px', h: '456px'};
    instance.triggered(dimensions);
    expect(spy).toHaveBeenCalledWith('123px', '456px');
  });

  it("triggers the callack when dimensions change", function () {
    var spy = jasmine.createSpy('spy');
    var instance = new AutoResizeAction(spy);
    var dimensions1 = {w: '123px', h: '456px'};
    var dimensions2 = {w: '321px', h: '654px'};
    instance.triggered(dimensions1);
    instance.triggered(dimensions2);
    expect(spy.calls.count()).toEqual(2);
  });


  it("triggers dont flicker", function () {
    var spy = jasmine.createSpy('spy');
    var instance = new AutoResizeAction(spy);
    var dimensions1 = {w: '123px', h: '456px'};
    var dimensions2 = {w: '122px', h: '355px'};
    instance.triggered(dimensions1);
    expect(spy.calls.count()).toEqual(1);
    instance.triggered(dimensions2);
    expect(spy.calls.count()).toEqual(2);

    instance.triggered(dimensions1);
    expect(spy.calls.count()).toEqual(3);
    expect(spy.calls.argsFor(2)).toEqual([dimensions1.w, dimensions1.h]);
    instance.triggered(dimensions2);
    expect(spy.calls.count()).toEqual(4);
    expect(spy.calls.argsFor(3)).toEqual([dimensions2.w, dimensions2.h]);
    // flicker here
    instance.triggered(dimensions1);
    expect(spy.calls.count()).toEqual(5);
    expect(spy.calls.argsFor(4)).toEqual(['100%', '456px']);

  });


});