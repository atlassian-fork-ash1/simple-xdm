import resizeListener from 'plugin/resize-listener';
describe("Resize Listener", function () {

  describe("Container Resize Listener", function () {
    var container, content;
    beforeEach(function(){
      content = document.createElement("div");

      container = document.createElement("div");
      container.id = 'content';
      container.appendChild(content);
      document.body.appendChild(container);

    });

    afterEach(function(){
      document.body.removeChild(container);
      resizeListener.remove();
    });

    it("add method adds callback to the queue", function () {
      var spy = jasmine.createSpy('spy');
      resizeListener.add(spy);
      expect(container.resizedAttached.q[0]).toBe(spy);
    });

    it("triggers the callback on dimension change", function (done) {
      var spy = jasmine.createSpy('spy');
      spy.and.callFake(function (){
        expect(spy).toHaveBeenCalled();
        done();
      });
      resizeListener.add(spy);
      content.style.height = '200px';
    });

  });

  describe("body resize listener", function () {
    var fixture;
    beforeEach(function(){
      resizeListener.remove();
      var containers = Array.prototype.slice.call(document.querySelectorAll("#content,.ac-content"));
      containers.forEach(function(element){
        element.parentNode.removeChild(element);
      });
      fixture = document.createElement('div');
      document.body.appendChild(fixture);
      resizeListener.remove();
    });

    afterEach(function(){
      fixture.parentNode.removeChild(fixture);
    });

    it("body triggers the callback on dimension change", function (done) {
      var spy = jasmine.createSpy('spy');
      spy.and.callFake(function (){
        expect(spy).toHaveBeenCalled();
        done();
      });
      resizeListener.add(spy);
      var div = document.createElement('div');
      div.style['padding-top'] = '10000px';
      fixture.appendChild(div);
    });

    it("margin and padding are removed from body", function () {
      document.body.style['padding-top'] = '20px';
      document.body.style['margin-top'] = '21px';
      document.body.style['padding-bottom'] = '22px';
      document.body.style['margin-bottom'] = '23px';
      resizeListener.add(function(){});
      expect(document.body.style['padding-top']).toEqual('0px');
      expect(document.body.style['margin-top']).toEqual('0px');
      expect(document.body.style['padding-bottom']).toEqual('0px');
      expect(document.body.style['margin-bottom']).toEqual('0px');
    });

  });

});