import size from 'plugin/size';
import ConfigurationOptions from 'plugin/configuration-options';

describe("Size", function () {
  var container;
  beforeEach(function(){
    container = document.createElement("div");
    container.id = 'content';
    document.body.appendChild(container);
  });

  afterEach(function(){
    document.body.removeChild(container);
  });

  it("size calculates 100% width by default", function () {
    var width = 120;
    container.setAttribute('style', 'width:' + width + 'px; height:1px;');
    var dimensions = size();
    expect(dimensions.w).toEqual("100%");
  });

  it("size calculates the correct height", function () {
    var height = 110;
    container.setAttribute('style', 'height:' + height + 'px; width:1px;');
    var dimensions = size();
    console.log('dims?', dimensions);
    expect(dimensions.h).toEqual(height);
  });

  it("size calculates the correct height with margin", function () {
    var height = 110;
    var marginBottom = 10;
    container.setAttribute('style', 'height:' + height + 'px; margin-bottom:' + marginBottom + 'px; width:1px;');
    var dimensions = size();
    expect(dimensions.h).toEqual(height + marginBottom);
  });

  it("size calculates the correct height with border", function () {
    var height = 110;
    var border = 4;
    container.setAttribute('style', 'height:' + height + 'px; border-top:' + border + 'px solid red; width:1px;');
    var dimensions = size();
    expect(dimensions.h).toEqual(height + border);
  });

  it("size calculates the correct height with padding", function () {
    var height = 110;
    var paddingTop = 4;
    container.setAttribute('style', 'height:' + height + 'px; padding-top:' + paddingTop + 'px; width:1px;');
    var dimensions = size();
    expect(dimensions.h).toEqual(height + paddingTop);
  });

  it("size calculates the correct height with padding + margin", function () {
    var height = 110;
    var paddingTop = 4;
    var marginTop = 4;
    container.setAttribute('style', 'height:' + height + 'px; padding-top:' + paddingTop + 'px; margin-top: ' + marginTop + 'px; width:1px;');
    var dimensions = size();
    expect(dimensions.h).toEqual(height + paddingTop + marginTop);
  });

  it("size gives the correct width with widthinpx", function () {
    var width = 120;
    ConfigurationOptions.set('widthinpx', true);
    container.setAttribute('style', 'width:' + width + 'px; height:1px;');
    var dimensions = size();
    expect(dimensions.w).toEqual(width);
  });

  describe('Scroll bars', function () {
    var iframe;
    var callback;
    var maxScrollbarThickness = 50;

    beforeEach(function () {
      iframe = document.createElement("iframe");
      iframe.setAttribute('onload', 'this.contentWindow.postMessage(encodeURI(' + size.toString() + '), "*")');
    });

    afterEach(function () {
      container.removeChild(iframe);
      window.removeEventListener('message', callback);
    });

    it("includes the height of a horizontal scroll bar in an iframe", function (done) {
      callback = e => {
        const heightInHTMLFile = 200;
        expect(parseInt(e.data.h)).toBeLessThan(heightInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.h)).toBeGreaterThan(heightInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec.html';
      container.appendChild(iframe);
    });

    it("includes the width of a vertical scroll bar in an iframe", function (done) {
      callback = e => {
        const widthInHTMLFile = 350;
        expect(parseInt(e.data.w)).toBeLessThan(widthInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.w)).toBeGreaterThan(widthInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec.html';
      container.appendChild(iframe);
    });

    it("includes the height of a horizontal scroll bar in an iframe with content just over the limit", function (done) {
      callback = e => {
        const heightInHTMLFile = 150 + 1;
        expect(parseInt(e.data.h)).toBeLessThan(heightInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.h)).toBeGreaterThan(heightInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec_limit.html';
      container.appendChild(iframe);
    });

    it("includes the width of a vertical scroll bar in an iframe with content just over the limit", function (done) {
      callback = e => {
        const widthInHTMLFile = 300 + 1;
        expect(parseInt(e.data.w)).toBeLessThan(widthInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.w)).toBeGreaterThan(widthInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec_limit.html';
      container.appendChild(iframe);
    });

    it("includes the height of a horizontal scroll bar in a very large iframe", function (done) {
      callback = e => {
        const heightInHTMLFile = 20000;
        expect(parseInt(e.data.h)).toBeLessThan(heightInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.h)).toBeGreaterThan(heightInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec_large.html';
      container.appendChild(iframe);
    });

    it("includes the width of a vertical scroll bar in a very large iframe", function (done) {
      callback = e => {
        const widthInHTMLFile = 20000;
        expect(parseInt(e.data.w)).toBeLessThan(widthInHTMLFile + maxScrollbarThickness);
        expect(parseInt(e.data.w)).toBeGreaterThan(widthInHTMLFile);
        done();
      };
      window.addEventListener('message', callback);
      iframe.src = window.location.origin + '/base/spec/assets/size_spec_large.html';
      container.appendChild(iframe);
    });

  });

});